<?php
// $Id$

/**
 * @file
 * Renders the google_images admin settings form.
 */

function google_images_admin() {
  $form = array();

  $form['google_images_picasa_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Picasa username'),
    '#default_value' => variable_get('google_images_picasa_username', 'kevincarbonaro'),
    '#size' => 30,
    '#maxlength' => 80,
    '#required' => TRUE,
    '#description' => t('Enter your Picasa Web Albums username.'),
  );

  $form['google_images_photo_maxwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum photo width'),
    '#default_value' => variable_get('google_images_photo_maxwidth', '250'),
    '#size' => 20,
    '#maxlength' => 3,
    '#element_validate' => array('google_images_maxwidth_validate'),
    '#description' => t('Enter the maximum display width in pixels for your photos. When a single photo is displayed, the original width of the photo might be too large. If the photo is larger than the specified width, it will be resized proportionally so that it does not look squashed or stretched.'),
);

  $form['google_images_use_lightbox'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use lightbox (adds rel="lightbox" attribute to photo links)'),
    '#default_value' => variable_get('google_images_use_lightbox', FALSE),
    '#description' => t('Enable / Disable the use of lightbox module to display photos.'),
  );

  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['advanced']['google_images_default_css'] = array(
    '#type' => 'textfield',
    '#title' => t('Override Default CSS Stylesheet'),
    '#default_value' => variable_get('google_images_default_css', 'Default'),
    '#maxlength' => 255,
    '#description' => t('Leave empty to use default CSS Stylesheet or enter a relative path to a custom CSS file (e.g. "/mypics.css"). Please read the documentation on how to set this up.'),
  );
  return system_settings_form($form);
}

function google_images_maxwidth_validate($element, &$form_state) {
  if (is_numeric($element['#value'])) {
  }
  else {
    form_error($element, t('The Maximum photo width field must contain a number.'));
  }
}